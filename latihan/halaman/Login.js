import React, {useState} from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View,StatusBar } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';


const Login = () => {

  const navigation = useNavigation()
  const [displaySosmed,setDisplaySosmed] = useState ("flex")
  const [nohp,setNohp] = useState ("")
  const [actionButton,setActionButton] = useState ("Or continue with a social media")


const proses = () => {
  if(nohp.length > 9 && nohp.length < 13 && nohp.substr(0,1) 
  == '8' && !isNaN(nohp)){
    
    // api sms
    // fetch('https://reqres.in/api/login', {
    //   method:'POST',
    //   headers: {
    //     'Accept' : 'application/json',
    //     'Content-Type' : 'application/json',
    //   },
    //   body:JSON.stringify({
    //     email : 'eve.holt@reqres.in',
    //     password : 'cityslicka',
    //   })
      
    // }).then((response) => response.json())
    // .then((responseJson) => {
    //   console.warn(responseJson)
    // })
        

    // pindah navigasi user
    navigation.navigate("VerifikasiOtp", {kodeOtp :123456, nohp : nohp})
  }
}


  return (
    <View style={{ flex:1, backgroundColor:"#32cd32" }}>
      
      <View style={{marginHorizontal:10, flex:1}}>
        <View style={{alignItems:'center', justifyContent:'center'}}>
          <Text style={{fontSize:35, color:'#fff', marginTop:50}}>Welcome To Your Account</Text>
        </View>
          <Text style={{fontSize:35, color:'#fff', marginTop:100}}>verify your number</Text>
          <Text style={{fontSize:16, fontWeight:'bold', color:'#fff', marginTop:20}}>Enter your mobile number to continue</Text>

        <View style={{width:300, height:50, backgroundColor:'#fff', marginTop:10, borderRadius:10}}>
          <TextInput style={{fontSize:19, marginHorizontal:10,}} 
            placeholder='Phone number'
            onChangeText={(nohp) => {
              setNohp(nohp)
              if(nohp.length > 9 && nohp.length < 13 && nohp.substr(0,1) 
              == '8' && !isNaN(nohp)) {
                setActionButton("Continue")
              } else {
                setActionButton ("Or continue with a social media")
              }
            }}
            keyboardType="number-pad"
            onFocus={() => setDisplaySosmed ("none")}
            onBlur={() => setDisplaySosmed ("flex")}
            value={nohp}


          />
        </View>
      </View>

          <TouchableRipple style={{alignItems:'center', marginBottom:15,}}
            onPress = {() => proses()}
          >
            <Text style={{fontSize:15, color:'#fff'}}>{actionButton}</Text>
          </TouchableRipple>

          <View style={{display:displaySosmed,height:150, backgroundColor:'#fff', flexDirection:'row', justifyContent:'space-around', borderTopRightRadius:35,borderTopLeftRadius:35}}>
            <TouchableRipple style={{backgroundColor:'#2166b0', width:100, height:50,justifyContent:'center',elevation:4,borderRadius:15, marginVertical:35,}}>
              <Text style={{color:'#fff',textAlign:'center'}}>Facebook</Text>
            </TouchableRipple>

            <TouchableRipple style={{backgroundColor:'#fff', width:100, height:50,justifyContent:'center',elevation:4,borderRadius:15,marginVertical:35}}>
              <Text style={{color:'grey',textAlign:'center'}}>Google</Text>
            </TouchableRipple>
          </View>
          

    </View>
  );
};

export default Login;

const styles = StyleSheet.create({

});