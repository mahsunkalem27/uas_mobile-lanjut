import React, {useState} from 'react';
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native';
import { TouchableRipple } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';


const VerifikasiOtp = (props) => {

  const navigation = useNavigation()
  const [displaySosmed,setDisplaySosmed] = useState ("flex")
  const [nohp,setNohp] = useState (props.route.params.nohp)
  const [verifikasiOtp,setverifikasiOtp] = useState (props.route.params.kodeOtp)
  const [actionButton,setActionButton] = useState ("Or continue with a social media")
  const [kodeOtp,setKodeOtp] = useState ("")
  const [warningError, setWarningError] = useState ("")



const proses = (kodeOtp) => {
  if(nohp.length > 5 ) {
    // pindah ke home

    if(kodeOtp ==  verifikasiOtp ) {
      setWarningError("INVALID CODE")
    }else{
      navigation.navigate('Beranda')
    }
  }else{
    setWarningError("")
  }
}


  return (
    <View style={{ flex:1, backgroundColor:"#32cd32" }}>
      <View style={{marginHorizontal:10, flex:1}}>
        <View style={{alignItems:'center', justifyContent:'center'}}>
          <Text style={{fontSize:40, color:'#fff', marginTop:50, }}>My Verification</Text>
        </View>
        
          <Text style={{fontSize:34, color:'#fff', marginTop:100, marginBottom:20}}>Enter 6-digit code send to</Text>
          <Text style={{fontSize:28, fontWeight:'bold', color:'#fff', marginVertical:15}}>{nohp}</Text>

        <View style={{width:300, height:50, backgroundColor:'#fff', marginTop:10, borderRadius:10}}>
          <TextInput style={{fontSize:19, marginHorizontal:10,}} 
            placeholder='Enter code number'
            onChangeText={(kodeOtp) => {

              setKodeOtp(kodeOtp)
              if(kodeOtp.length > 5 ) {
                proses()
              } 

            }}
            keyboardType="number-pad"
            value={kodeOtp}
          />
        </View>

        <View>
          <Text style={{color:'red', fontWeight:'bold', textAlign:'center', marginTop:15, fontSize:18}}>{warningError}</Text>
        </View>
      </View>
    </View>
  );
};

export default VerifikasiOtp;

const styles = StyleSheet.create({

});