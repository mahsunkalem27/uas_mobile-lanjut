import Home from "./Home";
import Activity from "./Activity";
import Payment from "./Payment";
import Account from "./Account";
import Inbox from "./Inbox";


export {Home, Activity, Payment, Account, Inbox,};