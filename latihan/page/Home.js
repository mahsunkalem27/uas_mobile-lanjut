import React from 'react';
import { 
  StyleSheet, 
  Text, 
  View,
  Dimensions, 
  ScrollView, 
  StatusBar, 
  Image,
  

} from 'react-native';

import OvoComponent from '../component/OvoComponent';
import FiturUtama from '../component/FiturUtama';
import PromoItems from '../component/PromoItems';

const {height,width} = Dimensions.get('window')
const Home = (props) => {
  return (
    <ScrollView style={{backgroundColor:'#fff'}}>
    
      
        <Image style={styles.imageBanner} source={require('../assets/image/awan.jpg')}/>
          <Text style={styles.greetingText}>Selamat Datang</Text>
  
          <View style={styles.wraperOvo}>
            <View style={styles.textOvo}>
              {/* <Text style={{fontSize:17, fontWeight:'bold', color:'#383838',}}>Ovo Balance</Text>
              <Text style={{fontSize:17, fontWeight:'bold', color:'#383838',}}>Rp 1000000</Text> */}
            </View>
            {/* garis bawah ovo */}
            <View style={styles.garisOvo}></View>
          <View/>
          {/*  gambar component ovo */}
          <OvoComponent/>
          </View>
          
          <View style={{}}>
          {/* <FiturUtama/> */}
          </View>
          <View style={styles.garispemisah}></View>
          <View style={styles.divider}>
          
          </View>
          <PromoItems/>
    </ScrollView>
  );
};

export default Home;

const styles = StyleSheet.create({
  imageBanner: {
  height:150,
  width:width,
  borderWidth:2, 
  borderColor:'#fff', 
  borderTopLeftRadius:10, 
  borderTopRightRadius:10,

},
greetingText: {
  fontSize:17,
  fontWeight:'bold',
  position:'absolute',
  alignSelf:'center',
  top:30,
  color:'#383838'
},
wraperOvo: {
  marginHorizontal:18,
  height:120,
  marginTop:-60,
  backgroundColor:'#fff',
  elevation:7,
  borderRadius:10,
},
textOvo: {
  flexDirection:'row',
  justifyContent:'space-between',
  marginHorizontal:12,
  marginTop:10,
},
garisOvo: {
  height:.8,
  backgroundColor:'#808080',
  marginTop:10,
},
divider: {
  width:width,
  // height:15,
  backgroundColor:'red',
  // marginVertical:15,
},
garispemisah: {
  height:15,
  backgroundColor:'#f5f5f5',
},
});