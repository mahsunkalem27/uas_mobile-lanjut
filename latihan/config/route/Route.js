import React from 'react';
import { createMaterialBottomTabNavigator,  } from '@react-navigation/material-bottom-tabs';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { Home, Activity, Payment, Inbox, Account, } from '../../page';
import { Food, Bike, Car, Delevery, Subcribe, Doctor, Pulsa, More, Login, VerifikasiOtp } from '../../halaman';
import Ionicons from 'react-native-vector-icons/Ionicons';


// stack navigator
const Stack = createNativeStackNavigator();
const Route = () => {
  return (
    <Stack.Navigator 
    // initialRouteName="Home"
    initialRouteName='Login'
    >
      <Stack.Screen
        name='Beranda'
        component={TabsButton}
        options={{ headerShown : false }}
      />
      <Stack.Screen
        name='Food'
        component={Food}
      />

      <Stack.Screen
        name='Bike'
        component={Bike}
      />

      <Stack.Screen
        name='Car'
        component={Car}
      />

      <Stack.Screen
        name='Delevery'
        component={Delevery}
      />

      <Stack.Screen
        name='Subcribe'
        component={Subcribe}
      />

      <Stack.Screen
        name='Doctor'
        component={Doctor}
      />

      <Stack.Screen
        name='Pulsa'
        component={Pulsa}
      />

      <Stack.Screen
        name='More'
        component={More}

      />

      {/* Login */}
      <Stack.Screen
        name='Login'
        component={Login}
        options={{ headerShown : false }}
      />

      {/* verifikasi */}
      <Stack.Screen
        name='VerifikasiOtp'
        component={VerifikasiOtp}
        options={{ headerShown : false }}
      />

    </Stack.Navigator>
  )
}

export default Route;


//tab bawah
const MaterialBottom = createMaterialBottomTabNavigator();

const TabsButton = () => {
  return (
    <MaterialBottom.Navigator 
    shifting={false}
    activeColor="#32cd32" //limegreen
    inactiveColor="#c0c0c0" //grey
    barStyle={{ backgroundColor: '#f5f5f5', paddingBottom:5 }}
    >
      <MaterialBottom.Screen
        name="Home" 
        component={Home} 
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons name="home" color={color} size={24} />
          ),
        }}
        
      />

      <MaterialBottom.Screen
        name="Activity" 
        component={Activity} 
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons name="newspaper" color={color} size={23}/>
          )
        }}
      />

      <MaterialBottom.Screen
        name="Payment" 
        component={Payment} 
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons name="file-tray" color={color} size={27}/>
          )
        }} 
      />

      <MaterialBottom.Screen
        name="Pesan" 
        component={Inbox} 
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons name="md-chatbubble-ellipses-outline" color={color} size={24}/>
          )
        }}
      />
      <MaterialBottom.Screen
        name="Account" 
        component={Account} 
        options={{
          tabBarIcon: ({ color }) => (
            <Ionicons name="person" color={color} size={23}/>
          )
        }} 
      />

    </MaterialBottom.Navigator>
    
  );
};


