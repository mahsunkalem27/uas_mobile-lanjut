import { NavigationContainer } from '@react-navigation/native';
import React from 'react';
import { Image, nav, Dimensions, StatusBar,StyleSheet, Text, ScrollView} from 'react-native';
import Route from './config/route/Route';


const App = () => {
  return (
    <NavigationContainer>
      <StatusBar backgroundColor='#32cd32'/>
      <Route/>
    </NavigationContainer>
  
  );
};

export default App;

const styles = StyleSheet.create({

});
