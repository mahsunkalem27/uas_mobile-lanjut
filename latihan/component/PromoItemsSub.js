import React from 'react';
import { StyleSheet, Text, View, Dimensions, Image } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';


const {height,width} = Dimensions.get('window')
const PromoItemsSub = (props) => {
  return (
    <View  style={styles.wrapper}>
      <Image source={props.image} style={styles.promoGambar}/>
        <View style={styles.textPromo}>
          <Text style={{fontWeight:'bold',fontSize:16}}>{props.text}</Text>
        </View>

        <View  style={styles.discon}>
          <Text>{props.diskon}</Text>
        </View>
        {/* icon */}
        <View style={styles.textBwhMakanan}>
          <Ionicons name='calendar' size={17} color='#808080' />
          <Text>{props.masaBerlaku}</Text>
        </View>
    </View>
  );
};

export default PromoItemsSub;

const styles = StyleSheet.create({
  wrapper: {
    backgroundColor:'#f5f5f5', //whitesmoke
    elevation:4, 
    borderRadius:8, 
    width:width/2 -27, 
    marginRight:18, 
    paddingBottom:3,
    marginVertical:10,
  },
  promoGambar: {
    height:width-170,
    width:width/2-27,
    borderTopLeftRadius:8,
    borderTopRightRadius:8
  },
  textPromo: {
    marginLeft:8,
    marginTop:3
  },
  discon: {
    position:'absolute',
    backgroundColor:'#fff', //putih
    borderRadius:3,
    height:20,
    marginLeft:5,
    marginTop:5,
    paddingHorizontal:5
  },
  textBwhMakanan: {
    marginHorizontal:10,
    marginVertical:4, 
    flexDirection:'row', 
    justifyContent:'space-between'
  },
});