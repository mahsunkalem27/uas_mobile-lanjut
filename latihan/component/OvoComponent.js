import React from 'react';
import { Image, Text, View, Dimensions, StyleSheet, StatusBar, TouchableOpacity} from 'react-native';


const {height,width} = Dimensions.get('window')
const OvoComponent = () => {
  
  return (
    // <View style={styles.wraperPay}>
    //   <TouchableOpacity>
    //     <Image style={styles.ovoFeatureImage} source={require('../assets/icon/pay.jpg')}/>
    //     <Text style={{alignSelf:'center',}}>Paylet</Text>
    //   </TouchableOpacity>

    //   <TouchableOpacity>
    //     <Image style={styles.ovoFeatureImage} source={require('../assets/icon/topup.jpg')}/>
    //     <Text style={{alignSelf:'center',}}>Top Up</Text>
    //   </TouchableOpacity>

    //   <TouchableOpacity>
    //     <Image style={styles.ovoFeatureImage} source={require('../assets/icon/reward.jpg')}/>
    //     <Text style={{alignSelf:'center',}}>Reward</Text>
    //   </TouchableOpacity>
    // </View>
    <View style={styles.wraperPay}>
      <TouchableOpacity>
        <Image style={styles.ovoFeatureImage} source={require('../assets/icon/pay.jpg')}/>
        <Text style={{alignSelf:'center',}}>Menu</Text>
      </TouchableOpacity>

      <TouchableOpacity>
        <Image style={styles.ovoFeatureImage} source={require('../assets/icon/topup.jpg')}/>
        <Text style={{alignSelf:'center',}}>Fvorite</Text>
      </TouchableOpacity>

      <TouchableOpacity>
        <Image style={styles.ovoFeatureImage} source={require('../assets/icon/reward.jpg')}/>
        <Text style={{alignSelf:'center',}}>Reward</Text>
      </TouchableOpacity>
    </View>
  );
};

export default OvoComponent;

const styles = StyleSheet.create({
  wraperPay: {
    flexDirection:'row',
    justifyContent:'space-between'
  },
  ovoFeatureImage: {
    height:30, 
    width:30, 
    marginTop:17, 
    marginHorizontal:30,
  },
});