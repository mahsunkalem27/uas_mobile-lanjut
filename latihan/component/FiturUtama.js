import React from 'react';
import { 
  Image, 
  Text, 
  View, 
  Dimensions, 
  StyleSheet, 
  StatusBar,
} from 'react-native';
import FiturutamaSub from './FiturUtamaSub';
import { useNavigation } from '@react-navigation/native';


const FiturUtama = () => {
  const Navigation = useNavigation();

  return (
    <View style={styles.wrapperfitur}>
      <FiturutamaSub onPress={() => Navigation.navigate('Food')} image={require('../assets/icon/food.jpg')} title="Food" />
      <FiturutamaSub onPress={() => Navigation.navigate('Bike')} image={require('../assets/icon/bike.jpg')} title="Bike"/>
      <FiturutamaSub onPress={() => Navigation.navigate('Car')} image={require('../assets/icon/car.jpg')} title="Car"/>
      <FiturutamaSub onPress={() => Navigation.navigate('Delevery')} image={require('../assets/icon/send.jpg')} title="Delevery"/>
      <FiturutamaSub onPress={() => Navigation.navigate('Subcribe')} image={require('../assets/icon/subscribe.jpg')} title="Subcribe" />
      <FiturutamaSub onPress={() => Navigation.navigate('Doctor')} image={require('../assets/icon/doctor.jpg')} title="Doctor"/>
      <FiturutamaSub onPress={() => Navigation.navigate('Pulsa')} image={require('../assets/icon/pulsa.jpg')} title="Pulsa"/>
      <FiturutamaSub onPress={() => Navigation.navigate('More')} image={require('../assets/icon/more.jpg')} title="More"/>
    </View>
  );
};

export default FiturUtama;

const styles = StyleSheet.create({
  wrapperfitur: {
    flexDirection:'row',
    justifyContent:'space-between',
    paddingVertical:10,
    flexWrap:'wrap',
    width:'100%',
  },
});